<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SolverController extends Controller
{
    public function solve(Request $request): JsonResponse
    {
        $validated = $request->input('data');

        $result = $this->sumOfNestedArray($validated);

        return response()->json(['result' => $result]);
    }

    public function sumOfNestedArray(array $arr, int $depth = 1)
    {
        $sum = 0;
        foreach ($arr as $item) {
            if (is_array($item)) {
                $sum += $this->sumOfNestedArray($item, $depth + 1) * $depth;
            } else {
                $sum += $depth * $item;
            }
        }

        return $sum;
    }
}
